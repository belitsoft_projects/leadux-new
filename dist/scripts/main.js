$(function() {
  $('.js-btn-toggle').on('click tap', function(){
    var dataToogleWrapper = $(this).attr('data-toggle-wrapper');
    $('body').toggleClass(dataToogleWrapper);
    return false;
  });
});

$(function() {
  $('.js-full-info').on('click tap', function(){
    $(this).toggleClass('active');
    $(this).parents('tr').next('.table__full-info-tr').toggleClass('active');
    return false;
  });
});

$(function() {
  $('.js-desctop-block-position__btn-group .btn__block-position').on('click tap', function(){
    $(this).siblings(".btn__active-position").removeClass('btn__active-position');
    $(this).addClass('btn__active-position');
  });
});

$(document).ready(function(){
  $('.mobile-table__carusel').owlCarousel({
    items:1,
    nav: true,
    navText: [
        '<i class="carusel-arrow carusel-arrow_right"></i>',
        '<i class="carusel-arrow carusel-arrow_left"></i>'
    ]
  });
});

function copyToClipboard(elementId) {

  // Create a "hidden" input
  var aux = document.createElement("input");

  // Assign it the value of the specified element
  aux.setAttribute("value", document.getElementById(elementId).innerHTML);

  // Append it to the body
  document.body.appendChild(aux);

  // Highlight its content
  aux.select();

  // Copy the highlighted text
  document.execCommand("copy");

  // Remove it from the body
  document.body.removeChild(aux);
}


$(function() {
  // Placeholder fix for IE
  $('.lt-ie10 [placeholder]').focus(function() {
    var i = $(this);
    if (i.val() == i.attr('placeholder')) {
      i.val('').removeClass('placeholder');
      if (i.hasClass('password')) {
        i.removeClass('password');
        this.type = 'password';
      }
    }
  }).blur(function() {
    if (i.val() == '' || i.val() == i.attr('placeholder')) {
      if (this.type == 'password') {
        i.addClass('password');
        this.type = 'text';
      }
      i.addClass('placeholder').val(i.attr('placeholder'));
    }
  }).blur().parents('form').submit(function() {
    //if($(this).validationEngine('validate')) { // If using validationEngine
    $(this).find('[placeholder]').each(function() {
        var i = $(this);
        if (i.val() == i.attr('placeholder'))
          i.val('');
        i.removeClass('placeholder');
      });
      //}
  });
});
